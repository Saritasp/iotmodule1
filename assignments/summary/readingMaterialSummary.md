# IoT(Internet of Things)
  ### What is IoT?

 ![IOT](Pictures/IoT-devices.jpg)

 * *Internet of Things* (IoT) is an ecosystem of `connected physical objects` that are accessible through the internet.
 * The embedded technology in the objects helps them to interact with internal states or the external environment, which in turn affects the decisions taken.

  ### Why IoT?

 * IoT enables devices/objects to observe, identify and understand a situation or the surroundings without being dependent on human help.

# IIoT(Industrial Internet of Things)

 ![IIOT](Pictures/industrial-internet-of-things.jpg)
 
 * *Industrial Internet of Things* (IIoT) refers to the application of IoT technology in `industrial settings`, especially with respect to instrumentation and control of sensors and devices that engage `cloud technologies`.

# Industrial Revolution
 ### * How it all started.....
 ![Revolution Of industry](Pictures/industry-4-0-timeline2.png)
 * **Industry 1.0 (1784):**  
    * Mechanisation, Steam power & Weaving loom.
 * **Industry 2.0 (1870):**  
    * Mass Production, Assembly line & Electrical energy.
 * **Industry 3.0 (1969):**  
    * Automation, Computers & Electronics.
    * Data are stored in databases and represented in excels.
 * **Industry 4.0 (Today):**  
    * Heavy usage of _Internet_.
    * Cyber Physical Systems, Internet of Things and Networking 

# Industry 1.0 :
 ![Industry 1.0](Pictures/logistics-I_1.jpg)
 ![Industry 1.0](Pictures/industry_1.gif)

# Industry 2.0 :
 ![Industry 2.0](Pictures/industry_2.2.jpg)
 ![Industry 2.0](Pictures/industry_2.0.gif)
 
# Industry 3.0 :
 * Industry 3.0 was a huge leap ahead wherein the advent of computer and automation ruled the industrial scene. It was during this period of transformation era where more and more robots were used in to the processes to perform the tasks which were performed by humans. 
 >E.g. Use of Programmable Logic Controllers(PLCs), Robots etc.

 ![Industry 3.0](Pictures/industry_3.0.gif)

 ## Industry 3.0 Architecture:
 ![Architecture](Pictures/333.png)

 * **Field Devices** - They are at the bottom of the Hierarchy. These are the sensors or actuators present in the factory, they measuring things or actuate machines (doing some actions). *Examples* are-
    *  sensors - A device which detects or measures a physical property and records, indicates, or otherwise responds to it.
    *  acutators - A device that causes a machine or other device to operate. 
 * **Control Devies** - They tell the field devices what to do and control them. *Examples* are-
    * PCs
    * PLCs(Programmable Logic Controller)
    * CNCs(Computer Numerical Control)
    * DCS(Distributed Control System)
 * **Station** - A set of Field devices & Control Devices together is called a station.
 * **Work Centres** - A set of stations together is called a work centre (Factories). They use *softwares* like 
    * Scada
    * Historian
 * **Enterprise** - A set of Work Centres together becomes an Enterprise. They use *softwares* like 
    * ERP
    * MES  
 The softwares used like Scada, Erp etc are used to store data, arrange data given to them by the field devices through the 
 control devices.

 ## Industry 3.0 Communication Protocol:
 ![Protocol](Pictures/protocol_3.png)

# Industry 4.0 :
 ![Industry 4.0](Pictures/industry_4.0.gif)

 ## Industry 4.0 Architecture:
 ![Architecture](Pictures/PF_RAMI-Grafiken_2016-01-15__2.jpg)

 ## Challenges faced by Industry 4.0:
 ![Challenges](Pictures/4.0_challenges.png)

  ### **However, there is a solution!**
 Get data from Industry 3.0 devices/meters/sensors without changes to the original device. Further, we can use Industry 4.0 devices to send data to the cloud.

 ## What is the difference between Industry 3.0 and Industry 4.0
 ![compare](Pictures/Comparison-3_4.png)

 ## Industry 4.0 Protocol:
 ![Protocol](Pictures/protocol_4.png)

 ## Converting Indusry 3.0 devices to Industry 4.0:
 To convert a 3.0 to 4.0, a library that helps, get data from industry 3.0 devices and send to industry 4.0 cloud.
  * Identify most popular industry 3.0 devices
  * Study protocols that these devices communicate
  * Get data from the industry 3.0 devices
  * Send the data to cloud for industry 4.0

  ## Industry 4.0 Conversion and Application 
  ![Conversion and Application](Pictures/Industry-4.0-the-convergence.jpg)

# Tools Used:

 > After the data is sent to the cloud the following processes can take place

 * **`TSDB Tools`**: Store data in _Time Shared Data database(TSDB)_ using tools like
    * Prometheus
    * InfluxDB
    * Things Board
    * Grafana

 * **`IOT Dashboards`**:View all your datas into beautiful dashboards.Iot dashboards available are:
    * Grafana 
    * Thingsboard

 * **`IOT Platforms`**: An IoT platform enables IoT device and endpoint management, connectivity and network management, data management, processing and analysis, application development, security, access control, monitoring, event processing and interfacing/integration. Analyse Your data in these platforms: 

 ![Platforms](Pictures/IoT_Platforms-03_0.jpg)

 * **`Get Alerts/Notifications`**:Gets alerts/SMS based on your data using these Platforms:   
   * Zaiper
   * Twilio 
  